from scipy import stats
confidence_interval = 0.95  # Confidence interval   (95%)
P = 1-confidence_interval   # Remaining probability ( 5%)
n = 30                      # Number of samples
f = n - 1                   # Degrees of fredom
t = stats.t.ppf(1-P/2, f)   # Correction factor
