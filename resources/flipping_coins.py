%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from numpy.random import randint

# Flipping 3 coins by computer 10 times
# 0 = "Tail", 1 = "Head" 
coins = 3
flips = 10
coin_tosses = pd.DataFrame(
    data = randint(2, size=(flips, coins))
)

# H = number of heads for each toss
#   is the sum of 0 and 1 in columns.
coin_tosses['H'] = coin_tosses.sum(axis='columns')

# Plot distribution P(H)
bins = np.arange(coins+2)
coin_tosses['H'].plot.hist(
    bins=bins,
    density=True,
    align='left'
)

plt.xticks(bins)
plt.title(f'Flipping {coins} coins {flips} times.')
plt.xlabel('Number of heads, $H$')
plt.ylabel('Distribution, $P(H)$')
