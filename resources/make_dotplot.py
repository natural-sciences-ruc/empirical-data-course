# Import Pandas and matplotlib modules.
import pandas as pd
import matplotlib.pyplot as plt

# DataFrame with data.
df = pd.DataFrame({
    'Infant mortality rate': [
        11.4, 44.7, 22.6, 7.7, 18.9, 20.9,
        30.0, 24.7, 28.6, 18.8, 11.3, 26.5
    ]
})

# Make dot plot.
y = df['Infant mortality rate']
plt.figure(figsize=[6, 0.5])  # New elongated figure.
plt.plot(   # Plot values agains zeros:
    y,      # - values on x-axis,
    y*0,    # - values on y-axis,
    'o'     # - make dots.
)
plt.yticks([])   # Remove ticks on y-axis.
plt.xlim(0, 50)  # Set limits on x-axis.
plt.xlabel('Infant mortality rate in percent')
plt.show()
