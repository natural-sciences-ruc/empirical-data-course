#!/usr/bin/python3
# -*- coding: utf-8 -*-
''' Test module to demonstrate how to make a module. '''


def subtract(x, y):
    ''' Subtract two input numbers '''
    result = x - y
    return result

def multiply(x, y):
    ''' Multiply two input numbers '''
    result = x * y
    return result
