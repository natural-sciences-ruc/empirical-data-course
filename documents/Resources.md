# Online resources for Jupyter/Python

List of on-line material that might be of interest. 

# User guides
The Python community have developed many modules 
for scientific computing.
This course uses the [Pandas](https://pandas.pydata.org/) module 
for data analysis, the [NumPy](https://numpy.org/) module 
for numerical calculations and the [Matplotlib](https://matplotlib.org/) for making plots.
These modules are a part of the [SciPy](https://www.scipy.org/) (pronounced “Sigh Pie”) ecosystem 
of open-source software for mathematics, science, and engineering. Below are links to user guides and tutorials

* [Jupyter Notebook User Documentation](https://jupyter-notebook.readthedocs.io/en/stable)
* [The Python Tutorial](https://docs.python.org/)
* [Pandas User Guide](https://pandas.pydata.org/docs/user_guide/index.html)
* [Matplotlib User Guide](https://matplotlib.org/users/index.html)
* [NumPy User Guide](https://numpy.org/doc/stable/user/index.html)
* [The SciPy Cookbook](https://scipy-cookbook.readthedocs.io)


## From Matlab to Python ##

- [Matlab to Python White Paper](https://www.enthought.com/wp-content/uploads/Enthought-MATLAB-to-Python-White-Paper.pdf). Start from chapter 2.
- [Numpy for Matlab users](https://docs.scipy.org/doc/numpy/user/numpy-for-matlab-users.html)


## Books and Teaching material written in Jupyter notebooks ##



Links are given as following types: 

- '**Web**': In most cases a web-page rendering Jupyter notebooks. This is the fastest way to acces the material, but you can not modify examples or repeat calculations in Jupyter.
- '**Youtube**': Related video.
- '**Notebooks+**': Acces to Jupyter notebooks. **Including** output from cells so that they can be read as is without running Jupyter/Python.  
- '**Notebooks-**': Acces to Jupyter notebooks. **Excluding** output from cells so that they need to executed in Jupyter/Python.  
- '**Binder**': gives acces to the notebooks via a free cloud-service (Might take some time to start up). Here you  can modify examples and repeat calculations in the Jupyter notebooks.

1. **"Python Data Science Handbook"** [Notebooks+](https://jakevdp.github.io/PythonDataScienceHandbook/). Links to Binder and Colab included. 
2. **"Enginering Computations" (4 Modules, 17 Lessons):** [Notebooks+](https://github.com/engineersCode/EngComp), Links to Binder embeded in Lessons.
2. **"CFD Python - 12 Steps to Navier-Stokes":** [Notebooks+](https://github.com/barbagroup/CFDPython), [Binder](https://mybinder.org/v2/gh/barbagroup/CFDPython/master)
3. **"Riemann Problems and Jupyter Solutions" (PDEs):** [Web](http://www.clawpack.org/riemann_book/html/Index.html#Riemann-Problems-and-Jupyter-Solutions), 
 [Youtube](https://www.youtube.com/watch?v=mC_RERKi56c), [Notebooks-](https://github.com/clawpack/riemann_book/blob/master/Index.ipynb), [Binder](https://mybinder.org/v2/gh/clawpack/riemann_book/master) (Navigate to "riemann_book" and open "Index.ipynb")
4. **"An Introduction to Applied Bioinformatics"**: [Web](http://readiab.org/book/latest/), [Notebooks-](http://readiab.org/), [Binder](https://mybinder.org/v2/gh/applied-bioinformatics/built-iab/master?filepath=IAB-notebooks/index.ipynb)

## More online resources:
* [Python Programming MOOC 2022](https://programming-22.mooc.fi)
* [Hands-on Scientific Computing](https://handsonscicomp.readthedocs.io/en/latest/)
* [Python for Everybody](https://www.py4e.com/)
* [Python for Data Science](https://www.edx.org/course/python-for-data-science-2). Below is a list of specific topics:
Free course (in "Audit mode") from UC San Diego 
- **Week 1: "Getting started with Data Science"**. An introduction to the field of Data Science - can be skipped at first if you want to get directly to Python.
- **Week 2: "Background in Python and Unix"**. Introduction to the core of Python. Last part of "Unix" can be skipped.
- **Week 3: "Jupyter and Numpy"**
- **Week 4: "Pandas"**
- **Week 5: "Data Visualization"**
- **Week 6: "Mini Project"**
- **Week 7: "Introduction to Machine Learning"**
- **Week 8: "Working with Text and Databases"**
- **Week 9+10: "Final Project"**




